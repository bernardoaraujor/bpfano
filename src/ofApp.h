#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

        void updateVideoState();
        void updateMesh();
        void updateLines(int t);
		void updateLogo();
		void updateLyric(int t);
		void updateAlpha(int start, int delay, int duration, float* alpha);

		void drawLyric(string verse, int x, int y, float* a);
		void drawLogo();
		void drawCircles(int t, bool backwards);
		void drawLinesR(int t, bool backwards);
		void drawLinesH(int t);
		void drawLinesV(int t);

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofVideoPlayer vA, vB, vC, vD, vE, vF, currentVideo;
        ofSoundPlayer music;
        ofFbo fbo, fbo2;
        ofMesh mesh;
        ofImage image, image2;
        ofPixels fboPixels, fboPixels2;
		ofImage logo;
		ofImage thef00l, thef00l2, milo, milo2;

    private:
        int vW = 1280;
        int vH = 720;

        int w = 177;
        int h = 100;
        int meshSize = 6;

		float alphaOcean = 0.0;
		int oceanTransitionT = 4000;
		int oceanD = 60000*2+31000+delayT;

		int logoStart = 4000;
		float logoTransitionT = 3000;
		float alphaLogo = 0.0;

		float lyricTransitionT = 1000;
		float alphaLyric = 0.0;
		float alphaLyric2 = 0.0;
		float alphaLyric3 = 0.0;

        int delayT = 10100;

        int introT = 10100 + delayT;
        int verseT = 28200 + delayT;
        int interT = 70500 + delayT;
        int soloT = 92300 + delayT;
        int fimT = 120600 + delayT;

        ofTrueTypeFont font;

/*        string verses[15] = {
			"      I've never seen\n something quite like this",
			"it pours in my brain coming \n     from holes in my skull",
			"it drips from this ocean \n     that I'm building",
        	"keeping my eyes \non the prize",
			"but now the road \nseems so long",
			"I've never had \nanything in my name",
			"social suicide you're \npracticing for so long",
			"it drips from this ocean \nthat I'm building",
			"keeping my eyes \non the prize",
			"but now the road \nseems so long!",
			"and even the memories",
			"I gave a heavy meaning",
			"now they seem more \nlike a fiction",
			"and less like \na real thing",
			"just like the \nmovies that you like"
		};
*/
		string verses[15] = {
			"     I'VE NEVER SEEN\n SOMETHING QUITE LIKE THIS",
			"IT POURS IN MY BRAIN COMING \n  FROM HOLES IN MY SKULL",
			"IT DRIPS FROM THIS OCEAN\n    THAT I'M BUILDING",
			"KEEPING MY EYES \nON THE PRIZE",
			"BUT NOW THE ROAD \nSEEMS SO LONG",
			"     I'VE NEVER HAD \nANYTHING IN MY NAME",
			" SOCIAL SUICIDE YOU'RE \nPRACTICING FOR SO LONG",
			"IT DRIPS FROM THIS OCEAN \n       THAT I'M BUILDING",
			"KEEPING MY EYES \nON THE PRIZE",
			"BUT NOW THE ROAD \nSEEMS SO LONG",
			"AND EVEN THE MEMORIES",
			"I GAVE A HEAVY MEANING",
			"NOW THEY SEEM MORE \nLIKE A FICTION",
			"AND LESS LIKE \nA REAL THING",
			"JUST LIKE THE \nMOVIES THAT YOU LIKE"
		};

        // start, duration
        int versesT[15][2] = {
        	{29000 + delayT, 3500}, //"I've never seen\n something quite like this",
			{32500 + delayT, 3500}, //"it pours in my brain coming \nfrom holes in my skull",
			{36000 + delayT, 2800}, //"it drips from this ocean \nthat I'm building",
			{38500 + delayT, 4000}, //"keeping my eyes \non the prize"
			{40000 + delayT, 4000}, //"but now the road \nseems so long"
			{57000 + delayT, 3500}, //"I've never had \nanything in my name",
			{60500 + delayT, 4000}, //"social suicide you're \npracticing for so long",
			{64500 + delayT, 3500}, //"it drips from this ocean \nthat I'm building",
			{67000 + delayT, 4000}, //"keeping my eyes \non the prize",
			{69000 + delayT, 4000}, //"but now the road \nseems so long",
			{77500 + delayT, 4000}, //"and even the memories \n",
			{79000 + delayT, 4000}, //I gave a heavy meaning
			{84500 + delayT, 3000}, //"now they seem more \nlike a fiction",
			{86000 + delayT, 3500}, //"and less like \na real thing",
			{90000 + delayT, 4000} // "just like the \nmovies that you like"
		};

		int startLines = delayT;
		int endLines = delayT + 14000;
		int startCircles = endLines;
		int circlesBackwards = delayT + 24500;
		int endCircles = verseT;

		int startBackwards = delayT + 13500;
		int endBackwards = delayT + 14000;

        int perlinWaveAmplitude = 20;
        int videoWaveDamp = 4;

        float tiltCurrent = 15;
        float tiltTarget = 15;
        float turnCurrent = 1;
        float turnTarget = 1;

};
