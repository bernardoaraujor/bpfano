#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(0);
    ofSetHexColor(0xffffff);

    fbo.allocate(vW, vH);
    fbo.begin();
    ofClear(255,255,255, 0);
    fbo.end();

    fbo2.allocate(vW, vH);
    fbo2.begin();
    ofClear(255,255,255, 0);
    fbo2.end();

    vA.loadMovie("a.mov");
    vB.loadMovie("b.mov");
    vC.loadMovie("c.mov");
    vD.loadMovie("d.mov");
    vE.loadMovie("e.mov");
    vF.loadMovie("f.mov");

    music.load("blueprints.mp3");
    music.play();

    font.load("font.ttf", 40);
    logo.load("logo.png");

    thef00l.load("THEF00L.png");
    thef00l2.load("THEF00L2.png");
    milo.load("milo.png");
    milo2.load("milo2.png");

    for (int y=0; y<h; y++) {
        for (int x=0; x<w; x++) {
            mesh.addVertex(ofPoint((x - w/2) * meshSize, (y - h/2) * meshSize, 0));
            mesh.addTexCoord(ofVec2f(x * (vW / w), y * (vH / h)));
            mesh.addColor(ofColor(255, 255, 255));
        }
    }

    for (int y=0; y<h-1; y++) {
        for (int x=0; x<w-1; x++) {
            int i1 = x + w * y;
            int i2 = x+1 + w * y;
            int i3 = x + w * (y+1);
            int i4 = x+1 + w * (y+1);
            mesh.addTriangle( i1, i2, i3 );
            mesh.addTriangle( i2, i4, i3 );
        }
    }

    ofEnableAlphaBlending();
    ofSetFrameRate(30);
}

//--------------------------------------------------------------
void ofApp::update(){
    updateVideoState();
    updateMesh();
}

//--------------------------------------------------------------
void ofApp::updateVideoState(){
    int t = ofGetElapsedTimeMillis();

    // which video should be played?
    if (t < introT) {
        currentVideo = vC;
        videoWaveDamp = 20;
        perlinWaveAmplitude = 30;
    }else if (t > introT && t < verseT){
        currentVideo = vD;
        vC.stop();
        vC.closeMovie();

        videoWaveDamp = 6;
        perlinWaveAmplitude = 20;
    }else if (t > verseT && t < interT){
        currentVideo = vA;
        vD.stop();
        vD.closeMovie();
    }else if (t > interT && t < soloT){
        currentVideo = vF;
        vA.stop();
        vA.closeMovie();
    }else if (t > soloT && t < fimT){
        currentVideo = vB;
        vF.stop();
        vF.closeMovie();

        videoWaveDamp = 3;
    }else if (t > fimT){
        currentVideo = vE;
        vB.stop();
        vB.closeMovie();
    }

    currentVideo.play();
    currentVideo.update();

    if (currentVideo.isFrameNew()){
        fbo.begin();
        currentVideo.draw(0, 0);
        fbo.end();

        fbo2.begin();
        ofSetColor(255, 255, 255, 255*alphaOcean);
        currentVideo.draw(0, 0);
        ofSetColor(255, 255, 255, 255);
        fbo2.end();

        // draw logo?
        if (t > logoStart && t < delayT) {
            updateLogo();
        }

        updateAlpha(0, oceanTransitionT, oceanD, &alphaOcean);

        updateLines(t);
        updateLyric(t);

        fbo.readToPixels(fboPixels);
        fbo2.readToPixels(fboPixels2);

        image.setFromPixels(fboPixels);
        image2.setFromPixels(fboPixels2);
    }


}
//--------------------------------------------------------------
void ofApp::updateLogo(){
    updateAlpha(logoStart, logoTransitionT, delayT-logoStart, &alphaLogo);
    drawLogo();
}

//--------------------------------------------------------------
void ofApp::updateMesh(){

    //Change vertices
    for (int y=0; y<h; y++) {
        for (int x=0; x<w; x++) {

            //Vertex index
            int i = x + w * y;
            ofPoint p = mesh.getVertex(i);

            float scaleX = vW/w;
            float scaleY = vH/h;

            int index = ((x * scaleX) + vW * (y * scaleY)) * 4; // FBO has four components (including Alpha)
            int brightness = fboPixels[index] / videoWaveDamp; // 4 is an arbitrary scalar to reduce the amount of distortion

            int wave = ofNoise(x * 0.05, y * 0.05, ofGetElapsedTimef() * 0.5) * perlinWaveAmplitude;

            //Change z-coordinate of vertex
            p.z = brightness + wave;
            mesh.setVertex(i, p);

            //Change color of vertex
            mesh.setColor(i , ofColor(255, 255, 255));
        }
    }
}

//--------------------------------------------------------------
void ofApp::updateLines(int t){
    if (t > startLines && t < endLines) {
        drawLinesV(t);
        drawLinesH(t);
    }else if(t > startCircles && t < circlesBackwards){
        drawCircles(t, false);
        drawLinesR(t, true);
    }else if(t > circlesBackwards && t < endCircles){
        drawCircles(t, true);
        drawLinesR(t, false);
    }
}
//--------------------------------------------------------------
void ofApp::updateLyric(int t){

    if (t > versesT[0][0] && t < versesT[0][0] + versesT[0][1]) {
        updateAlpha(versesT[0][0], lyricTransitionT, versesT[0][1], &alphaLyric);
        drawLyric(verses[0], 70, 150, &alphaLyric);
    }

    if (t > versesT[1][0] && t < versesT[1][0] + versesT[1][1]) {
        updateAlpha(versesT[1][0], lyricTransitionT, versesT[1][1], &alphaLyric2);
        drawLyric(verses[1], 50, 300, &alphaLyric2);
    }

    if (t > versesT[2][0] && t < versesT[2][0] + versesT[2][1]) {
        updateAlpha(versesT[2][0], lyricTransitionT, versesT[2][1], &alphaLyric3);
        drawLyric(verses[2], 120, 500, &alphaLyric3);
    }

    if (t > versesT[3][0] && t < versesT[3][0] + versesT[3][1]) {
        updateAlpha(versesT[3][0], lyricTransitionT, versesT[3][1], &alphaLyric);
        drawLyric(verses[3], 100, 200, &alphaLyric);
    }

    if (t > versesT[4][0] && t < versesT[4][0] + versesT[4][1]) {
        updateAlpha(versesT[4][0], lyricTransitionT, versesT[4][1], &alphaLyric2);
        drawLyric(verses[4], 100, 500, &alphaLyric2);
    }

    if (t > versesT[5][0] && t < versesT[5][0] + versesT[5][1]) {
        updateAlpha(versesT[5][0], lyricTransitionT, versesT[5][1], &alphaLyric3);
        drawLyric(verses[5], 310, 150, &alphaLyric3);
    }

    if (t > versesT[6][0] && t < versesT[6][0] + versesT[6][1]) {
        updateAlpha(versesT[6][0], lyricTransitionT, versesT[6][1], &alphaLyric);
        drawLyric(verses[6], 180, 350, &alphaLyric);
    }

    if (t > versesT[7][0] && t < versesT[7][0] + versesT[7][1]) {
        updateAlpha(versesT[7][0], lyricTransitionT, versesT[7][1], &alphaLyric2);
        drawLyric(verses[7], 90, 500, &alphaLyric2);
    }

    if (t > versesT[8][0] && t < versesT[8][0] + versesT[8][1]) {
        updateAlpha(versesT[8][0], lyricTransitionT, versesT[8][1], &alphaLyric3);
        drawLyric(verses[8], 100, 200, &alphaLyric3);
    }

    if (t > versesT[9][0] && t < versesT[9][0] + versesT[9][1]) {
        updateAlpha(versesT[9][0], lyricTransitionT, versesT[9][1], &alphaLyric);
        drawLyric(verses[9], 100, 500, &alphaLyric);
    }

    if (t > versesT[10][0] && t < versesT[10][0] + versesT[10][1]) {
        updateAlpha(versesT[10][0], lyricTransitionT, versesT[10][1], &alphaLyric2);
        drawLyric(verses[10], 150, 350, &alphaLyric2);
    }

    if (t > versesT[11][0] && t < versesT[11][0] + versesT[11][1]) {
        updateAlpha(versesT[11][0], lyricTransitionT, versesT[11][1], &alphaLyric3);
        drawLyric(verses[11], 180, 500, &alphaLyric3);
    }

    if (t > versesT[12][0] && t < versesT[12][0] + versesT[12][1]) {
        updateAlpha(versesT[12][0], lyricTransitionT, versesT[12][1], &alphaLyric);
        drawLyric(verses[12], 100, 150, &alphaLyric);
    }

    if (t > versesT[13][0] && t < versesT[13][0] + versesT[13][1]) {
        updateAlpha(versesT[13][0], lyricTransitionT, versesT[13][1], &alphaLyric2);
        drawLyric(verses[13], 100, 350, &alphaLyric2);
    }

    if (t > versesT[14][0] && t < versesT[14][0] + versesT[14][1]) {
        updateAlpha(versesT[14][0], lyricTransitionT, versesT[14][1], &alphaLyric3);
        drawLyric(verses[14], 100, 500, &alphaLyric3);
    }
}

//--------------------------------------------------------------
void ofApp::updateAlpha(int start, int delay, int duration, float* alpha){
    int end = start + duration;
    int t = ofGetElapsedTimeMillis();

    if (t > start && t <= start + delay) {
        *alpha = (float)(t - start)/delay;
    }else if (t > start + delay && t <= end - delay) {
        *alpha = 1.0;
    }else if (t > (end - delay) && t < end) {
        *alpha = (float)(end - t)/delay;
    }
}
//--------------------------------------------------------------
void ofApp::drawCircles(int t, bool backwards){
    fbo2.begin();

    ofNoFill();
    ofEnableAntiAliasing();
    ofSetCircleResolution(1000);

    int nCircles = 13;

    int cX = 150;
    int cY = 150;
    int stepR = 100;

    float circleSpeed;
    if (!backwards){
        circleSpeed = 0.1;
    }else{
        circleSpeed = -0.1;
    }

    ofSetColor(255,255,255,255*0.7);

    //draw circles
    for (int i = 0; i < 13; i++){
        int r = i*stepR + (int)(circleSpeed*t)%100;
        ofDrawCircle(cX, cY, r);
    }

    ofSetColor(255,255,255,255);

    fbo2.end();
}

void ofApp::drawLinesR(int t, bool backwards){
    fbo2.begin();

    int cX = 150;
    int cY = 150;

    float lineSpeed;
    if (!backwards){
        lineSpeed = 0.05;
    }else{
        lineSpeed = -0.05;
    }

    ofSetColor(255,255,255,255*0.7);

    float stepAngle = 360/8;
    for (int i = 0; i < 8; i++){
        int angle = i*stepAngle + lineSpeed*t;

        ofPushMatrix();
        ofTranslate(cX, cY, 0);
        ofRotateDeg(angle);
        ofDrawLine(0,0,1000,1000);
        ofPopMatrix();
    }

    ofSetColor(255,255,255,255);

    fbo2.end();
}

void ofApp::drawLinesH(int t){
    fbo2.begin();

    ofVec2f v, v2;
    v.set(1, 0);
    v2 = 1000*v;

    int stepX = 200;
    int stepY = 80;

    float speedLines = 1;

    ofSetColor(255,255,255,255*0.7);

    for (int i = 0; i < 15; i++){
        ofPushMatrix();
        int delta = (int)speedLines*t%6000;
        ofTranslate(-3000 + delta + i*stepX, i*stepY, 0);
        ofDrawLine(v, v2);
        ofPopMatrix();
    }

    ofSetColor(255,255,255,255);

    fbo2.end();
}

void ofApp::drawLinesV(int t){
    fbo2.begin();

    ofVec2f v, v2;
    v.set(0, 1);
    v2 = 1000*v;

    int stepX = 200;
    int stepY = 80;

    float speedLines = 1;

    ofSetColor(255,255,255,255*0.7);

    for (int i = 0; i < 15; i++){
        ofPushMatrix();
        int delta = (int)speedLines*t%8000;
        ofTranslate(i*stepY, -6000 + delta + i*stepX, 0);
        ofDrawLine(v, v2);
        ofPopMatrix();
    }

    ofSetColor(255,255,255,255);

    fbo2.end();
}

//--------------------------------------------------------------
void ofApp::drawLyric(string verse, int x, int y, float* a) {
    fbo2.begin();
    ofSetColor(255, 255, 255, 255*(*a)*0.8);
    font.drawString(verse, x, y);
    ofSetColor(255, 255, 255, 255);
    fbo2.end();
}

//--------------------------------------------------------------
void ofApp::drawLogo(){
    fbo2.begin();
    ofSetColor(255, 255, 255, 255*alphaLogo);

    logo.draw(300, 60, 600, 600);

    if (((int)ofGetElapsedTimeMillis()/1000)%2){
        thef00l.draw(5, fbo2.getHeight()/2 - thef00l.getWidth()/2.5, thef00l.getWidth()*0.7, thef00l.getHeight()*0.7);
        milo.draw(930, fbo2.getHeight()/2 - milo.getHeight()/4, milo.getWidth()*0.5, milo.getHeight()*0.5);

    }else{
        thef00l2.draw(5, fbo2.getHeight()/2 - thef00l.getWidth()/2.5, thef00l.getWidth()*0.7, thef00l.getHeight()*0.7);
        milo2.draw(930, fbo2.getHeight()/2 - milo.getHeight()/4, milo.getWidth()*0.5, milo.getHeight()*0.5);
    }

    ofSetColor(255, 255, 255, 255);
    fbo2.end();
}

//--------------------------------------------------------------
void ofApp::draw(){

    ofPushMatrix();
    ofTranslate( ofGetWidth()/2, ofGetHeight()/2, 0 );

    tiltCurrent = ofLerp(tiltCurrent, tiltTarget, 0.1);
    turnCurrent = ofLerp(turnCurrent, turnTarget, 0.1);
    ofRotateX(tiltCurrent);
    ofRotateZ(turnCurrent);
    ofScale(1.3);

    image2.bind();
    mesh.draw();
    image2.unbind();

    //mesh.drawWireframe();

    ofPopMatrix();

    //ofDrawBitmapString(ofGetElapsedTimeMillis(),15,15);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    if (key == OF_KEY_DOWN){
        tiltTarget -= 5;
    } else if (key == OF_KEY_UP){
        tiltTarget += 5;
    } else if (key == OF_KEY_LEFT){
        turnTarget -= 5;
    } else if (key == OF_KEY_RIGHT){
        turnTarget += 5;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
